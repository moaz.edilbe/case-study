<!DOCTYPE html>
<html>

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        Production Line for car factory
    </title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    <style>
        /* text align for the body */
        body {
            text-align: center;
        }

        /* image dimension */
        img {
            height: 200px;
            width: 350px;
        }

        /* imagelistId styling */
        #imageListId {
            margin: 0;
            padding: 0;
            list-style-type: none;
        }

        #imageListId div {
            margin: 0 4px 4px 4px;
            padding: 0.4em;
            display: inline-block;
        }

        /* Output order styling */
        #outputvalues {
            margin: 0 2px 2px 2px;
            padding: 0.4em;
            padding-left: 1.5em;
            width: 250px;
            border: 2px solid green;
            background: gray;
        }

        .listMachine {
            border: 1px solid #006400;
            width: 350px;
        }

        .height {
            height: 10px;
        }
    </style>

    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function () {
            $("#imageListId").sortable({
                update: function (event, ui) {
                    getIdsOfImages();
                }//end update
            });
        });

        function getIdsOfImages() {
            var values = [];
            $('.listMachine').each(function (index) {
                values.push($(this).attr("id")
                    .replace("No", ""));
            });

            $('#outputvalues').val(values);
            ChangeOrder(values);

        }
    </script>
    <script>
        function Delete(element) {
            var div = $(element).closest("div")
            // div.remove();
            var index = div.attr("id").replace("No", "");
            $.ajax({
                url: "{{url('delete_machine_by_index/')}}/" + index,
                type: "POST",
                data: {},
                success: function () {
                    // alert("Delete");
                    location.reload();
                },
                error: function () {
                }
            });
        }

        function Add(element) {

            $.ajax({
                url: "{{url('add_new_machine/')}}",
                type: "POST",
                success: function () {
                    // alert("Add");
                    location.reload();
                },
                error: function () {
                }
            });
        }

        function ChangeOrder(values) {
            $.ajax({
                url: "{{url('change_order_machines/')}}",
                type: "POST",
                data: {
                    data: values
                },
                success: function () {
                    // alert("Change");
                    location.reload();
                },
                error: function () {
                }
            });
        }

        function ResetStorage() {

            $.ajax({
                url: "{{url('reset_storage/')}}",
                type: "POST",
                success: function () {
                    // alert("Reset");
                    location.reload();
                },
                error: function () {
                }
            });
        }

        function Run(element) {

            $('.inputs').each(function () {
                var value = $(this).attr("id").replace("No", "");
                // Run solution for this input
                $('.listMachine').each(function () {
                    var machine = $(this).attr("id").replace("No", "");

                    $.ajax({
                        url: "{{url('run_input_on_machine/')}}",
                        type: "POST",
                        data: {
                            input: value,
                            machine: machine,
                        },
                        success: function (data) {
                            data = data.message;
                            var str = data;
                            $("#progress").append("<div>" + str + " </div>");
                            // location.reload();
                        },
                        error: function () {
                        }
                    });
                });
            });
        }

        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }
    </script>

</head>

<body>
<button id="reset" class="reset" onClick="ResetStorage()">Reset Storage</button>

<div class="height"></div>
<br>

<div id="imageListId">
    <?php foreach ($Production_Line->machines as $key => $item){?>
    <div id="No{{$key}}" class="listMachine">
        <input type="text" name="" id="" value="{{$item->name}}"/>
        <button onClick="Delete(this)">
            <i class="fa fa-trash" aria-hidden="true"></i>
        </button>
    </div>
    <?php }?>
</div>


<button id="addMachine" class="push-to-add" onClick="Add(this)">Push to add</button>

<div id="outputDiv" style="display: none">
    <b>Output of ID's: </b>
    <input id="outputvalues" type="text" value=""/>
</div>


<br/>
<br/>
<br/>

<div>
    <?php foreach ($Production_Line->inputsQueue as $key => $item){?>
    <div id="No{{$key}}" class="inputs">
        <div>{{$item->color}}</div>
    </div>
    <?php }?>
</div>
<button id="run" class="run" onClick="Run(this)">Run</button>


<div id="progress">

    <H1>Progress div</H1>

</div>
</body>

</html>
