# Case Study

- Drag and drop the Machines to change orders.
- Can add new machine.
- Can delete machines.
- Inputs it is static in code.
- Inputs mean first input for first machine in Production Line.
- When input start on the line you can't change machines order. 
- In progress div you can see how do products go on the production line.
- The result for each machine is same input for ease of experimentation.
- Stored in storage instead of database.
- The solution not optimized because there is not enough time.


## Class Diagram
![](public/Design.png)

## Main Page
![](public/index.png)
