<?php

namespace App\Http\Controllers;

use App\Repositories\AssemblingRepository;
use App\Repositories\DesignRepository;
use App\Repositories\LineVariableRepository;
use App\Repositories\PaintingRepository;
use App\Repositories\PrintingRepository;
use App\Repositories\SortingRepository;
use App\Repositories\TestingRepository;
use App\Services\ProductionLine;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public static $machines = [
        "Design",
        "Printing",
        "Sorting",
        "Assembling",
        "Painting",
        "Testing",
    ];
    public static $inputs = [
        "black",
        "red",
        "blue",
    ];


    public function index()
    {
        $machines = Session::get('machines');
        if (!$machines) $machines = self::$machines;

        $PL = $this->getProductionLine($machines);

//        $PL->runSolution();
        Session::put('machines', $machines);
        return view("home", ["Production_Line" => $PL]);
    }


    public function add_new_machine()
    {
        $machines = Session::get('machines');
        $machines[] = "Testing";
        $PL = $this->getProductionLine($machines);
        Session::put('machines', $machines);
        return "Done";
    }

    public function delete_machine_by_index($index)
    {
        $machines = Session::get('machines');
        array_splice($machines, $index, 1);
        $PL = $this->getProductionLine($machines);
        Session::put('machines', $machines);
        return "Done";
    }

    public function change_order_machines(Request $request)
    {
        $values = $request->data;
        $machines = Session::get('machines');
        $new_machines = [];
        foreach ($values as $key) $new_machines[] = $machines[$key];
        $PL = $this->getProductionLine($new_machines);
        Session::put('machines', $new_machines);
        return "Done";
    }

    public function run_input_on_machine(Request $request)
    {
        $input = $request->input;
        $input_name = self::$inputs[$input];
        $machine_id = $request->machine;
        $machines = Session::get('machines');
        $PL = $this->getProductionLine($machines);
        $machine = $machines[$machine_id];
        $result = $PL->runSolutionOnInputForMachine(new LineVariableRepository($input_name, 100, 100, 100), $machine);
        // TODO save result to database

        return response()->json([
            "message" => "run " . $input_name . " on machine " . $machine . " with result: " . $result->color,
            "result" => $result->color,
        ]);
    }


    public function reset_storage()
    {
        Session::put('machines', self::$machines);
        return "Done";
    }
    public function getProductionLine($machines)
    {
        $PL = new ProductionLine();

        foreach ($machines as $machine) {
            $PL->addMachine(self::getMachineByName($machine));
        }

        foreach (self::$inputs as $input){
            $PL->addInput(new LineVariableRepository($input, 100, 100, 100));
        }
        return $PL;
    }

    static public function getMachineByName($name)
    {
        switch ($name) {
            case "Design":
                return new DesignRepository();
            case "Printing":
                return new PrintingRepository();
            case "Sorting":
                return new SortingRepository();
            case "Assembling":
                return new AssemblingRepository();
            case "Painting":
                return new PaintingRepository();
            case "Testing":
                return new TestingRepository();
        }

    }

}
