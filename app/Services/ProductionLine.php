<?php

namespace App\Services;

use App\Repositories\Interfaces\MachineRepositoryInterface;
use App\Repositories\LineVariableRepository;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;


class ProductionLine
{
    public $machines = [];
    public $inputsQueue = [];
    public $doneQueue = [];


    public function addMachine(MachineRepositoryInterface $machine)
    {
        $this->machines[] = $machine;
    }

    public function deleteMachine($index)
    {
        if (key_exists($index, $this->machines)) {
            array_splice($this->machines, $index, 1);
            return true;
        }
        return false;
    }

    public function changeOrder($new_machines)
    {
        $this->machines = $new_machines;
    }

    public function addInput(LineVariableRepository $input)
    {
        $this->inputsQueue[] = $input;
    }

    public function runSolution()
    {
        while (sizeof($this->inputsQueue)) {
            $input = $this->inputsQueue[0];

            $machines = $this->machines;
            $machines = Session::get('machines');

            $result = $input;
            foreach ($machines as $machine) {
                $machine = Controller::getMachineByName($machine);
                $machine->setInputs($result);
                $machine->produceResults();
                $result = $machine->getResults();
                $input->machine_result[$machine->name] = $result;
            }

            $input->result = $result;

            $this->doneQueue[] = $input;
            array_splice($this->inputsQueue, 0, 1);

            $second_to_sleep = 2;
//            error_log("sleep $second_to_sleep seconds");
//            echo "sleep $second_to_sleep seconds ";
//          sleep($second_to_sleep);
        }
    }

    public function runSolutionOnInput($input)
    {
        $machines = $this->machines;
        $machines = Session::get('machines');

        $result = $input;
        foreach ($machines as $machine) {
            $machine = Controller::getMachineByName($machine);
            $machine->setInputs($result);
            $machine->produceResults();
            $result = $machine->getResults();
            $input->machine_result[$machine->name] = $result;
        }

        $input->result = $result;

        $this->doneQueue[] = $input;
        array_splice($this->inputsQueue, 0, 1);

        $second_to_sleep = 2;
//        error_log("sleep $second_to_sleep seconds");
//        echo "sleep $second_to_sleep seconds ";
//        sleep($second_to_sleep);
    }

    public function runSolutionOnInputForMachine($input, $machine_name)
    {
        $machine = Controller::getMachineByName($machine_name);
        $machine->setInputs($input);
        $machine->produceResults();
        $result = $machine->getResults();

        $input->result = $result;


        $second_to_sleep = 1;
//        error_log("sleep $second_to_sleep seconds");
//        echo "sleep $second_to_sleep seconds ";
        sleep($second_to_sleep);

        return $result;
    }
}
