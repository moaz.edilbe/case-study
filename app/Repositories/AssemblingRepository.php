<?php

namespace App\Repositories;

use App\Repositories\Interfaces\MachineRepositoryInterface;

class AssemblingRepository implements MachineRepositoryInterface
{

    public $name = "Assembling";

    public $inputs;

    public function setInputs($inputs)
    {
        // TODO: Implement setInputs() method.
        $this->inputs = $inputs;
    }

    public function produceResults()
    {
        // TODO: Implement produceResults() method.

    }

    public function getResults()
    {
        // TODO: Implement getResults() method.
        return $this->inputs;
    }
}
