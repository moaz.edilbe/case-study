<?php

namespace App\Repositories\Interfaces;

interface MachineRepositoryInterface
{
    public function setInputs($inputs);

    public function produceResults();

    public function getResults();
}
