<?php

namespace App\Repositories;

class LineVariableRepository
{

    public $color = "Black";
    public $width = 100;
    public $length = 100;
    public $height = 100;

    public $result = null;
    public $machine_result = [];

    /**
     * @param string $color
     * @param int $width
     * @param int $length
     * @param int $height
     */
    public function __construct(string $color, int $width, int $length, int $height)
    {
        $this->color = $color;
        $this->width = $width;
        $this->length = $length;
        $this->height = $height;
    }

}
