<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [\App\Http\Controllers\Controller::class, "index"]);
Route::post('/add_new_machine', [\App\Http\Controllers\Controller::class, "add_new_machine"]);
Route::post('/delete_machine_by_index/{index}', [\App\Http\Controllers\Controller::class, "delete_machine_by_index"]);
Route::post('/change_order_machines', [\App\Http\Controllers\Controller::class, "change_order_machines"]);
Route::post('/run_input_on_machine', [\App\Http\Controllers\Controller::class, "run_input_on_machine"]);
Route::post('/reset_storage', [\App\Http\Controllers\Controller::class, "reset_storage"]);

//Route::get('/', function () {
//    return view('welcome');
//});
